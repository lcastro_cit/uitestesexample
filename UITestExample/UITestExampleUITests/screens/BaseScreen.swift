//
//  BaseScreen.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest

class BaseScreen {
    var app: XCUIApplication
    var mainElement: Element
    //var textManager = IMSelfieStaticTextManager.shared
    
    var isActive: Bool {
        return mainElement.view.waitForExistence(timeout: 2)
    }
    
    init?<T>(_ application: XCUIApplication, mainView: T, timeout: Double = 1) where T: ScreenElement, T: RawRepresentable {
        self.app = application
        self.mainElement = Element(rawValue: mainView.rawValue)
        
        if !mainView.view.waitForExistence(timeout: timeout) {
            return nil
        }
    }
    
    /// check if a view has a text
    ///
    /// - Parameters:
    ///   - string: the text fo find
    ///   - timeout: a timout
    /// - Returns: true if found
    func hasText(_ string: String, timeout: Double = 0.1) -> Bool {
        let q = app.staticTexts.element(matching: NSPredicate(format: "label CONTAINS '\(string)'")).firstMatch
        return q.waitForExistence(timeout: timeout)
    }
    
    /// returns a button from navigation
    ///
    /// - Parameter text: the text fo find the navigation
    /// - Returns: the element found
    func buttonFromNavigation(_ text: String) -> XCUIElement {
        return app.navigationBars[text].children(matching: .button).element
    }
    
    /// looks for a cell containing a given text
    ///
    /// - Parameter string: the text fo find
    /// - Returns: the cell
    func cellContaining(_ string: String) -> XCUIElement {
        return app.cells.containing(NSPredicate(format: "label CONTAINS '\(string)'")).firstMatch
    }
}





