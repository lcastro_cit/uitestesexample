//
//  LoginScreen.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest

class LoginScreen: BaseScreen {
    init?(_ application: XCUIApplication) {
        super.init(application, mainView: ScreenItem.loginScreen, timeout: 20)
    }
    
    enum ScreenItem: String, ScreenElement {
        case loginScreen
        case usernameTextField
        case passwordTextField
        case loginButton
    }
    
    func tapLogin() {
        ScreenItem.loginButton.button.tap()
    }
    
    func enterUserName() {
        ScreenItem.usernameTextField.textField.tap()
        let user = DataPool.shared.getString(.validUser)
        ScreenItem.usernameTextField.textField.typeText(user)
    }
    
    func enterPassword() {
        ScreenItem.passwordTextField.secureTextField.tap()
        let password = DataPool.shared.getString(.validPassword)
        ScreenItem.passwordTextField.secureTextField.typeText(password)
    }
}
