//
//  FriendsView.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest

class FriendsScreen: BaseScreen {
    init?(_ application: XCUIApplication) {
        super.init(application, mainView: ScreenItem.friendsScreen, timeout: 20)
    }
    
    enum ScreenItem: String, ScreenElement {
        case friendsScreen
        case cell_0
        case cell_1
    }
    
    func hasTwoFriends() -> Bool {
        let firstCellMatch = cellContaining(DataPool.shared.getString(.firstUserCellTitle))
        let secondCellMatch = cellContaining(DataPool.shared.getString(.secondUserCellTitle))
    
        return firstCellMatch.exists && secondCellMatch.exists
    }
    
    func logOut() {
        app.navigationBars.buttons["logOut"].tap()
    }
    
    static func waitForScreen(_ app: XCUIApplication) -> FriendsScreen? {
        let loginScreen = LoginScreen(app)
        loginScreen?.enterUserName()
        loginScreen?.enterPassword()
        loginScreen?.tapLogin()
        
        let friendsScreen = FriendsScreen(app)
        return friendsScreen
    }
}
