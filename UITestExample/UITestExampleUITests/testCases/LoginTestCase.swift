//
//  LoginTestCase.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest

protocol LoginBasicScenarioFeature {
    func givenThatIamOnLogin()
    func whenIFillTheFormWithValidUserAndValidPasswordAndTapToLogin()
    func thenIShouldSeeAListOfFriends()
}

protocol LoginAlternativeScenarioFeature {
    func givenThatIamOnFriensListScreen()
    func whenITapLogoutButton()
    func thenIShouldSeeLoginPageAgain()
}


protocol LoginFeature: LoginBasicScenarioFeature, LoginAlternativeScenarioFeature {
    func testValidUserShouldBeAbleToLoginAndSeeFriendsList()
    func testUserShouldbeAbleToLogout()
}


class LoginTestCase: BaseTestCase, LoginFeature {
    
    func testValidUserShouldBeAbleToLoginAndSeeFriendsList() {
        givenThatIamOnLogin()
        whenIFillTheFormWithValidUserAndValidPasswordAndTapToLogin()
        thenIShouldSeeAListOfFriends()
    }
    
    func testUserShouldbeAbleToLogout() {
        givenThatIamOnFriensListScreen()
        whenITapLogoutButton()
        thenIShouldSeeLoginPageAgain()
    }
    
    func givenThatIamOnFriensListScreen() {
        let friendsScreen = FriendsScreen.waitForScreen(app)
        XCTAssertScreen(friendsScreen)
    }
    
    func whenITapLogoutButton() {
        let friendsScreen = FriendsScreen(app)
        XCTAssertScreen(friendsScreen)
        friendsScreen?.logOut()
    }
    
    func thenIShouldSeeLoginPageAgain() {
        let loginScreen = LoginScreen(app)
        XCTAssertScreen(loginScreen)
    }
    
    func givenThatIamOnLogin() {
        let loginScreen = LoginScreen(app)
        XCTAssertScreen(loginScreen)
    }
    
    func whenIFillTheFormWithValidUserAndValidPasswordAndTapToLogin() {
        let loginScreen = LoginScreen(app)
        XCTAssertScreen(loginScreen)
        loginScreen?.enterUserName()
        loginScreen?.enterPassword()
        loginScreen?.tapLogin()
    }
    
    func thenIShouldSeeAListOfFriends() {
        let friendsScreen = FriendsScreen(app)
        XCTAssertScreen(friendsScreen)
    }
    
    
    func testValidUserShouldBeAbleToLogin() {
        let loginScreen = LoginScreen(app)
        XCTAssertScreen(loginScreen)
        loginScreen?.enterUserName()
        loginScreen?.enterPassword()
        loginScreen?.tapLogin()

        let friendsScreen = FriendsScreen(app)
        XCTAssertScreen(friendsScreen)
    }
}
