//
//  BaseTestCase.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest

class BaseTestCase: XCTestCase {
    var app: XCUIApplication!
    
    override open func setUp() {
        super.setUp()
        app = Application.app
        continueAfterFailure = false
        app.terminate()
        app.launch()
    }
    
    /// Teardown method called after the invocation of each test method in the class.
    override open func tearDown() {
        app.terminate()
        app = nil
        super.tearDown()
    }
}
