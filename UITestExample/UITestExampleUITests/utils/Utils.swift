//
//  Utils.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest


struct Application {
    static let app = XCUIApplication()
}

public extension XCUIElementQuery {
    internal subscript<T>(key: T) -> XCUIElement where T: RawRepresentable, T.RawValue == String { return self[key.rawValue] }
}

func XCTAssertScreen(_ screen: BaseScreen?, file: StaticString = #file, line: UInt = #line) {
    XCTAssertTrue(screen?.isActive ?? false, file: file, line: line)
}

protocol ScreenElement { }

extension ScreenElement where Self: RawRepresentable {
    typealias RawValue = String
    
    private var app: XCUIApplication {
        return Application.app
    }
    
    var view: XCUIElement {
        return any.matching(identifier: self.rawValue).firstMatch
    }
    
    var any: XCUIElementQuery {
        return app.descendants(matching: .any)
    }
    
    var button: XCUIElement {
        return app.buttons[self]
    }
    
    var textField: XCUIElement {
        return app.textFields[self]
    }
    
    var secureTextField: XCUIElement {
        return app.secureTextFields[self]
    }
    
    var exists: Bool {
        return view.exists
    }
    
}
