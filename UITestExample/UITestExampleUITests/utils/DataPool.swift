//
//  DataPool.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import Foundation

enum DataPoolItem: String {
    case validUser
    case validPassword
    case firstUserCellTitle
    case secondUserCellTitle
}
class DataPool {
    private var items = [DataPoolItem: Any]()
    static var shared = DataPool()
    init() {
        items[.validUser] = "traveller@gmail.com"
        items[.validPassword] = "123456"
        items[.firstUserCellTitle] = "Bike user"
        items[.secondUserCellTitle] = "Tesouro"
    }
    
    func getString(_ item: DataPoolItem) -> String {
        return items[item] as! String
    }
}
