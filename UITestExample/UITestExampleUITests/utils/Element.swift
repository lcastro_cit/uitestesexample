//
//  Element.swift
//  UITestExampleUITests
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import XCTest

struct Element: ScreenElement, RawRepresentable {
    init(rawValue: String) {
        self.rawValue = rawValue
    }
    
    var rawValue: String
    
    typealias RawValue = String
}
