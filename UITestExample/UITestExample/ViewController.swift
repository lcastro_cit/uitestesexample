//
//  ViewController.swift
//  UITestExample
//
//  Created by Luciano de Castro Martins on 26/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import UIKit
import Firebase

fileprivate func setCommon(_ txtField: UITextField) {
    txtField.translatesAutoresizingMaskIntoConstraints = false
    txtField.font = UIFont.boldSystemFont(ofSize: 14)
    txtField.backgroundColor = UIColor(white: 0, alpha: 0.09)
    txtField.borderStyle = .roundedRect
}

class ViewController: UIViewController {
    var topContraint: NSLayoutConstraint? = nil
    
    let usernameTextField: UITextField = {
       let txtField = UITextField()
        txtField.placeholder = "username"
        setCommon(txtField)
        txtField.accessibilityIdentifier = "usernameTextField"
        return txtField
    }()
    
    let passwordTextField: UITextField = {
       let txtField = UITextField()
        txtField.placeholder = "password"
        txtField.isSecureTextEntry = true
        setCommon(txtField)
        txtField.accessibilityIdentifier = "passwordTextField"
        return txtField
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("logIn", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor(red: 66/255, green: 182/255, blue: 244/255, alpha: 1)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        button.accessibilityIdentifier = "loginButton"
        return button
    }()
    
    
    @objc func handleLogin() {
        self.topContraint?.constant = -100
        updateViewConstraints()
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn], animations: {
            self.view.layoutIfNeeded()
        }) { _ in
            guard let email = self.usernameTextField.text, let password = self.passwordTextField.text else { return }
            Auth.auth().signIn(withEmail: email, password: password) { (user, err) in
                if let error = err {
                    print("error when loggin user: ", error)
                    return
                }
                print("user logged sucessfully")
                let layout = UICollectionViewFlowLayout()
                let nextControler = FriendsController(collectionViewLayout: layout)
                self.navigationController?.pushViewController(nextControler, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "loginScreen"
        view.backgroundColor = .white
        
        view.addSubview(usernameTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        addConstraints()
    }
    override func viewWillAppear(_ animated: Bool) {
        topContraint?.constant = -200
        topContraint?.isActive = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        topContraint?.isActive = false
    }
    
    fileprivate func addConstraints() {
        //usernameTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 40).isActive = true
        
        topContraint = usernameTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -200)
        usernameTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32).isActive = true
        usernameTextField.bottomAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: -10).isActive = true
        usernameTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32).isActive = true
        usernameTextField.heightAnchor.constraint(equalToConstant: 80)
        
        passwordTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32).isActive = true
        passwordTextField.bottomAnchor.constraint(equalTo: loginButton.topAnchor, constant: -10).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 80)
        
        //loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 10).isActive = true
        loginButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32).isActive = true
        loginButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}

