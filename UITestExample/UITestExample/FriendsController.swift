//
//  FriendsController.swift
//  UITestExample
//
//  Created by Luciano de Castro Martins on 27/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import UIKit
import Firebase

class FriendsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var friends = [Friend]()
    let friendCellId = "friendCellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.contentInset = UIEdgeInsetsMake(30, 0, 0, 0)
        collectionView?.backgroundColor = UIColor(red: 242/255, green: 238/255, blue: 236/255, alpha: 1)
        collectionView?.register(FriendCell.self, forCellWithReuseIdentifier: friendCellId)
        view.accessibilityIdentifier = "friendsScreen"
        getFriends()
        addLogoutButton()
    }
    
    fileprivate func addLogoutButton() {
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "logOut", style: .plain, target: self, action: #selector(handleLogout))
    }
    @objc func handleLogout(){
        do {
            try Auth.auth().signOut()
            
        } catch {
            print("error signin out")
        }
        navigationController?.popViewController(animated: true)
    }
    fileprivate func getFriends() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        Database.database().reference().child("following").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            dictionary.forEach({ (key, value) in
                Database.database().reference().child("users").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                    guard let user = snapshot.value as? [String: Any] else { return }
                    let friend = Friend(dictionary: user)
                    self.friends.append(friend)
                    self.collectionView?.reloadData()
                }, withCancel: { (error) in
                    print("an error ocurred when trying to get users")
                })
            })
        }) { (err) in
            print("an error ocurred when tried to get user friends")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: friendCellId, for: indexPath) as! FriendCell
        cell.friend = friends[indexPath.item]
        cell.accessibilityIdentifier = "cell_\(indexPath.item)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 32, height: 200)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 50
    }
    
}

struct Friend {
    let name: String
    let profileImageUrl: String
    
    init(dictionary: [String: Any]) {
        name = dictionary["username"] as? String ?? ""
        profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
    }
}

class FriendCell: UICollectionViewCell {
    
    var friend: Friend? {
        didSet {
            label.text = friend?.name
        }
    }
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 30)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 3
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 5, height: 10)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 2
        addSubview(label)
        backgroundColor = .white
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 100).isActive = true
        label.widthAnchor.constraint(equalToConstant: 200).isActive = true
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
